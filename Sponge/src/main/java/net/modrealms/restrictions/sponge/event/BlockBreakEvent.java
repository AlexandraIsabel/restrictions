package net.modrealms.restrictions.sponge.event;

import net.modrealms.restrictions.sponge.Restrictions;
import net.modrealms.restrictions.sponge.object.Restriction;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.cause.EventContextKeys;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BlockBreakEvent {

    @Listener(order = Order.FIRST, beforeModifications = true)
    public void onBlockBreak(ChangeBlockEvent.Break.Pre event, @Root Player player){
        if(player.hasPermission("modrealms.restrictions.bypass.break")) return;
        List<ItemStack> items = new ArrayList<>();
        for(Location<World> loc : event.getLocations()){
            try {
                ItemStack stack = ItemStack.builder().fromBlockSnapshot(loc.createSnapshot()).build();
                items.add(stack);
            } catch (Exception ignored) {

            }
        }
        Optional<Restriction> restrictionOptional = Restrictions.getInstance().getRestrictionManager().getRestrictionByItems(items);
        if(restrictionOptional.isPresent() && !restrictionOptional.get().getWhitelist().contains(Restrictions.getInstance().getCore().getDaoManager().getModpackDAO().getModpack().getTag()) && restrictionOptional.get().getActions().contains("BREAK")){
            event.setCancelled(true);
            player.sendMessage(Text.builder().append(Text.of(TextColors.DARK_RED, "Sorry! You cannot break this block because it is restricted!"))
                    .append(Text.NEW_LINE)
                    .append(Text.of(TextColors.GOLD, "Reason: ", TextColors.GRAY, restrictionOptional.get().getReason()))
                    .build());
        }
    }
}
