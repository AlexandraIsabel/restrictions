package net.modrealms.restrictions.sponge.event;

import net.modrealms.restrictions.sponge.Restrictions;
import net.modrealms.restrictions.sponge.object.Restriction;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.event.item.inventory.ChangeInventoryEvent;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ItemPickupEvent {

    @Listener(order = Order.FIRST, beforeModifications = true)
    public void onItemPickup(ChangeInventoryEvent.Pickup.Pre event, @Root Player player){
        if(player.hasPermission("modrealms.restrictions.bypass.pickup")) return;
        Optional<Restriction> restrictionOptional = Restrictions.getInstance().getRestrictionManager().getRestrictionByItem(event.getOriginalStack().createStack());
        if(restrictionOptional.isPresent() && !restrictionOptional.get().getWhitelist().contains(Restrictions.getInstance().getCore().getDaoManager().getModpackDAO().getModpack().getTag()) && restrictionOptional.get().getActions().contains("PICKUP")){
            event.setCancelled(true);
            player.sendMessage(Text.builder().append(Text.of(TextColors.DARK_RED, "Sorry! You cannot pickup this item because it is restricted!"))
                    .append(Text.NEW_LINE)
                    .append(Text.of(TextColors.GOLD, "Reason: ", TextColors.GRAY, restrictionOptional.get().getReason()))
                    .build());
        }
    }
}
