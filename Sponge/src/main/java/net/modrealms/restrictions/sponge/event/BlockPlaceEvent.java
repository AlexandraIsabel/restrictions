package net.modrealms.restrictions.sponge.event;

import net.modrealms.core.util.JSONUtil;
import net.modrealms.restrictions.sponge.Restrictions;
import net.modrealms.restrictions.sponge.object.Restriction;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.cause.EventContextKey;
import org.spongepowered.api.event.cause.EventContextKeys;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BlockPlaceEvent {

    @Listener(order = Order.FIRST, beforeModifications = true)
    public void onBlockPlace(ChangeBlockEvent.Place.Pre event, @Root Player player){
        if(player.hasPermission("modrealms.restrictions.bypass.place")) return;
        Optional<ItemStackSnapshot> itemStackSnapshotOptional = event.getContext().get(EventContextKeys.USED_ITEM);
        if(itemStackSnapshotOptional.isPresent()){
            ItemStack itemStack = itemStackSnapshotOptional.get().createStack();
            Optional<Restriction> restrictionOptional = Restrictions.getInstance().getRestrictionManager().getRestrictionByItem(itemStack);
            if(restrictionOptional.isPresent() && !restrictionOptional.get().getWhitelist().contains(Restrictions.getInstance().getCore().getDaoManager().getModpackDAO().getModpack().getTag()) && restrictionOptional.get().getActions().contains("PLACE")){
                event.setCancelled(true);
                player.sendMessage(Text.builder().append(Text.of(TextColors.DARK_RED, "Sorry! You cannot place this block because it is restricted!"))
                        .append(Text.NEW_LINE)
                        .append(Text.of(TextColors.GOLD, "Reason: ", TextColors.GRAY, restrictionOptional.get().getReason()))
                        .build());
            }
        }
    }
}
