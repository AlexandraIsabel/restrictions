package net.modrealms.restrictions.sponge;

import lombok.Getter;
import lombok.Setter;
import net.modrealms.connectionbridge.sponge.ConnectionBridge;
import net.modrealms.core.Core;
import net.modrealms.core.messenger.SpongeMessenger;
import net.modrealms.restrictions.sponge.command.RestrictionsCommand;
import net.modrealms.restrictions.sponge.event.*;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.game.state.GameAboutToStartServerEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.plugin.Dependency;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.plugin.PluginContainer;

import javax.inject.Inject;
import java.nio.file.Path;

@Plugin(id = "restriction", name = "MRRestrictions", version = "1.0", description = "Sponge plugin for handling item and block restrictions throughout the network.", dependencies = {@Dependency(id = "connectionbridge")})
public class Restrictions {
    @Inject
    @Getter
    @Setter
    @ConfigDir(sharedRoot = false)
    private Path configDir;

    @Inject
    private PluginContainer container;
    @Getter
    private RestrictionManager restrictionManager;
    @Getter
    private SpongeMessenger spongeMessenger;

    private static Restrictions plugin;

    public static Restrictions getInstance(){
        return plugin;
    }

    @Listener(order = Order.DEFAULT)
    public void onServerStartedEvent(GameStartedServerEvent event){
        plugin = this;

        this.getCore().getLogger().info("Restrictions is starting...");
        this.getCore().getMongo().getMorphia().mapPackage("net.modrealms.restrictions.sponge.object");

        this.restrictionManager = new RestrictionManager();
        this.spongeMessenger = new SpongeMessenger(this.container);

        Sponge.getCommandManager().register(this, new RestrictionsCommand(), "restrictions", "banneditems", "whatsbanned");

        Sponge.getEventManager().registerListeners(this, new BlockBreakEvent());
        Sponge.getEventManager().registerListeners(this, new BlockInteractEvent());
        Sponge.getEventManager().registerListeners(this, new BlockPlaceEvent());
        Sponge.getEventManager().registerListeners(this, new BlockModifyEvent());
        Sponge.getEventManager().registerListeners(this, new ItemDropEvent());
        Sponge.getEventManager().registerListeners(this, new ItemPickupEvent());
        Sponge.getEventManager().registerListeners(this, new ItemUseEvent());
        Sponge.getEventManager().registerListeners(this, new ItemCraftEvent());
        // Sponge.getEventManager().registerListeners(this, new EntityDeathEvent()); // turn off death messages
    }

    public Core getCore(){
        return Core.getInstance();
    }
}
