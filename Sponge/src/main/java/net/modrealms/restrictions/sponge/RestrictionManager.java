package net.modrealms.restrictions.sponge;

import lombok.Data;
import net.modrealms.core.util.JSONUtil;
import net.modrealms.restrictions.sponge.object.Restriction;
import org.spongepowered.api.item.inventory.ItemStack;
import xyz.morphia.Datastore;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
public class RestrictionManager {
    private List<Restriction> restrictionsList;

    private final Datastore datastore = Restrictions.getInstance().getCore().getMongo().getDatastore();

    public RestrictionManager(){
        this.restrictionsList = new ArrayList<>();
        sync();
    }

    public Optional<Restriction> getRestrictionByItem(ItemStack itemStack) {
        return this.restrictionsList.stream().filter(r -> !r.isNote() && r.equals(itemStack)).findAny();
    }

    public void updateRestriction(Restriction restriction){
        this.restrictionsList.removeIf(r -> r.getId().equals(restriction.getId()));
        this.restrictionsList.add(restriction);
        this.datastore.save(restriction);
    }

    public void deleteRestriction(Restriction restriction){
        this.restrictionsList.removeIf(r -> r.getId().equals(restriction.getId()));
        this.datastore.delete(restriction);
    }

    public void sync(){
        this.restrictionsList = this.datastore.createQuery(Restriction.class).asList();
    }

    public Optional<Restriction> getRestrictionByItems(List<ItemStack> itemStacks){
        for(ItemStack i : itemStacks){
            if(getRestrictionByItem(i).isPresent()){
                return getRestrictionByItem(i);
            }
        }
        return Optional.empty();
    }
}
