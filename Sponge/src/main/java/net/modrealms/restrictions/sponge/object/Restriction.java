package net.modrealms.restrictions.sponge.object;

import lombok.Data;
import net.modrealms.core.util.JSONUtil;
import net.modrealms.restrictions.sponge.Restrictions;
import org.bson.types.ObjectId;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.item.inventory.ItemStack;
import xyz.morphia.annotations.Entity;
import xyz.morphia.annotations.Id;
import xyz.morphia.annotations.Property;

import java.awt.image.DataBuffer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
@Entity(noClassnameStored = true, value = "restrictions")
public class Restriction {
    @Id
    @Property("_id")
    private ObjectId id;

    @Property("item_json")
    private String item;

    private boolean exact;

    private String reason;

    private boolean note;

    @Property(value = "modpack_whitelist")
    private List<String> whitelist;

    @Property(value = "restricted_actions")
    private List<String> actions;

    public Restriction(){
        this.id = new ObjectId();
        this.exact = false;
        this.note = false;
        this.actions = new ArrayList<String>(){{
            add("BREAK");
            add("INTERACT");
            add("MODIFY");
            add("PLACE");
            add("DROP");
            add("PICKUP");
            add("USE");
            add("CRAFT");
        }};
    }

    public List<String> getWhitelist(){
        if(this.whitelist == null){
            this.whitelist = new ArrayList<>();
        }
        return this.whitelist;
    }

    public List<String> getActions(){
        if(this.actions == null){
            this.actions = new ArrayList<>();
        }
        return this.actions;
    }

    public boolean equals(ItemStack itemStack){
        Optional<ItemStack> itemStackOptional = JSONUtil.getItemStackFromJSON(this.item);
        if(itemStackOptional.isPresent()){
            ItemStack restrictedStack = itemStackOptional.get();
            if(exact) {
                // check if it has the same type and unsafedata
                try {
                    return this.item.equalsIgnoreCase(JSONUtil.getItemstackJSON(itemStack));
                } catch (Exception e) {
                    Restrictions.getInstance().getCore().getLogger().error("We cannot get the itemstack json for " + itemStack.getType());
                    return false;
                }
            } else {
                if(restrictedStack.getType() == itemStack.getType()) {
                    return restrictedStack.toContainer().get(DataQuery.of("UnsafeDamage")).orElse(0).equals(itemStack.toContainer().get(DataQuery.of("UnsafeDamage")).orElse(0));
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }
}
