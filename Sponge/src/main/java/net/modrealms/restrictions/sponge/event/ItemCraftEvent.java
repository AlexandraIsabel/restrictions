package net.modrealms.restrictions.sponge.event;

import net.modrealms.restrictions.sponge.Restrictions;
import net.modrealms.restrictions.sponge.object.Restriction;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.entity.DestructEntityEvent;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.event.item.inventory.CraftItemEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;

public class ItemCraftEvent {
    @Listener(order = Order.DEFAULT, beforeModifications = true)
    public void onItemCraft(CraftItemEvent.Craft event, @Root Player player){
        if(player.hasPermission("modrealms.restrictions.bypass.craft")) return;
        event.getSlot().ifPresent(slot -> {
            Optional<ItemStack> item = slot.peek();
            if(item.isPresent()){
                Optional<Restriction> restrictionOptional = Restrictions.getInstance().getRestrictionManager().getRestrictionByItem(item.get());
                if(restrictionOptional.isPresent() && !restrictionOptional.get().getWhitelist().contains(Restrictions.getInstance().getCore().getDaoManager().getModpackDAO().getModpack().getTag()) && restrictionOptional.get().getActions().contains("CRAFT")){
                    event.setCancelled(true);
                    player.sendMessage(Text.builder().append(Text.of(TextColors.DARK_RED, "Sorry! You cannot craft this item because it is restricted!"))
                            .append(Text.NEW_LINE)
                            .append(Text.of(TextColors.GOLD, "Reason: ", TextColors.GRAY, restrictionOptional.get().getReason()))
                            .build());
                }
            }
        });
    }
}
