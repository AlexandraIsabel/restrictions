package net.modrealms.restrictions.sponge.event;

import net.modrealms.restrictions.sponge.Restrictions;
import net.modrealms.restrictions.sponge.object.Restriction;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.cause.EventContext;
import org.spongepowered.api.event.cause.EventContextKeys;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.event.item.inventory.ClickInventoryEvent;
import org.spongepowered.api.event.item.inventory.InteractInventoryEvent;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BlockInteractEvent {

    @Listener(order = Order.FIRST, beforeModifications = true)
    public void onBlockInteract(InteractBlockEvent.Secondary event, @Root Player player){
        if(player.hasPermission("modrealms.restrictions.bypass.interact")) return;
        ItemStack itemStack;
        try {
            itemStack = ItemStack.builder().fromBlockSnapshot(event.getTargetBlock()).build();
        } catch (IllegalArgumentException e){
            itemStack = ItemStack.empty();
        }
        Optional<Restriction> restrictionOptional = Restrictions.getInstance().getRestrictionManager().getRestrictionByItem(itemStack);
        if(restrictionOptional.isPresent() && !restrictionOptional.get().getWhitelist().contains(Restrictions.getInstance().getCore().getDaoManager().getModpackDAO().getModpack().getTag()) && restrictionOptional.get().getActions().contains("INTERACT")){
            event.setCancelled(true);
            player.sendMessage(Text.builder().append(Text.of(TextColors.DARK_RED, "Sorry! You cannot interact with this block because it is restricted!"))
                    .append(Text.NEW_LINE)
                    .append(Text.of(TextColors.GOLD, "Reason: ", TextColors.GRAY, restrictionOptional.get().getReason()))
                    .build());
        }
    }

    @Listener(order = Order.FIRST, beforeModifications = true)
    public void onOpenInventory(InteractInventoryEvent.Open event, @Root Player player){
        if(player.hasPermission("modrealms.restrictions.bypass.interact")) return;
        ItemStack itemStack;
        try {
            itemStack = ItemStack.builder().fromBlockSnapshot(event.getCause().getContext().get(EventContextKeys.BLOCK_HIT).orElse(BlockSnapshot.NONE)).build();
        } catch (IllegalArgumentException e){
            itemStack = ItemStack.empty();
        }
        Optional<Restriction> restrictionOptional = Restrictions.getInstance().getRestrictionManager().getRestrictionByItem(itemStack);
        if(restrictionOptional.isPresent() && !restrictionOptional.get().getWhitelist().contains(Restrictions.getInstance().getCore().getDaoManager().getModpackDAO().getModpack().getTag()) && restrictionOptional.get().getActions().contains("INTERACT")){
            event.setCancelled(true);
            player.sendMessage(Text.builder().append(Text.of(TextColors.DARK_RED, "Sorry! You cannot interact with this block because it is restricted!"))
                    .append(Text.NEW_LINE)
                    .append(Text.of(TextColors.GOLD, "Reason: ", TextColors.GRAY, restrictionOptional.get().getReason()))
                    .build());
        }
    }
}
