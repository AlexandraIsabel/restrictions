package net.modrealms.restrictions.sponge.event;

import net.modrealms.restrictions.sponge.Restrictions;
import net.modrealms.restrictions.sponge.object.Restriction;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.cause.EventContextKeys;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BlockModifyEvent {

    @Listener(order = Order.FIRST, beforeModifications = true)
    public void onBlockModify(ChangeBlockEvent.Modify event, @Root Player player){
        if(player.hasPermission("modrealms.restrictions.bypass.modify")) return;
        Optional<Restriction> restrictionOptional = Optional.empty();
        Optional<ItemStackSnapshot> itemStackSnapshotOptional = event.getContext().get(EventContextKeys.USED_ITEM);
        if(itemStackSnapshotOptional.isPresent()) {
            ItemStack itemStack = itemStackSnapshotOptional.get().createStack();
            restrictionOptional = Restrictions.getInstance().getRestrictionManager().getRestrictionByItem(itemStack);
        }
        if(!restrictionOptional.isPresent()){
            List<ItemStack> itemStacks = new ArrayList<>(event.getTransactions().stream().map(b -> {
                try {
                    return ItemStack.builder().fromBlockSnapshot(b.getOriginal()).build();
                } catch (IllegalArgumentException ignored) {}
                return ItemStack.empty();
            }).collect(Collectors.toList()));
            restrictionOptional = Restrictions.getInstance().getRestrictionManager().getRestrictionByItems(itemStacks);
        }
        if(restrictionOptional.isPresent() && !restrictionOptional.get().getWhitelist().contains(Restrictions.getInstance().getCore().getDaoManager().getModpackDAO().getModpack().getTag()) && restrictionOptional.get().getActions().contains("MODIFY")){
            event.setCancelled(true);
            player.sendMessage(Text.builder().append(Text.of(TextColors.DARK_RED, "Sorry! You cannot modify this block because it is restricted!"))
                    .append(Text.NEW_LINE)
                    .append(Text.of(TextColors.GOLD, "Reason: ", TextColors.GRAY, restrictionOptional.get().getReason()))
                    .build());
        }
    }
}
