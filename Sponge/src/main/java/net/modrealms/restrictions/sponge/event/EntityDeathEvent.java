package net.modrealms.restrictions.sponge.event;

import net.modrealms.core.Core;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.entity.DestructEntityEvent;

public class EntityDeathEvent {
    @Listener(order = Order.DEFAULT, beforeModifications = true)
    public void onDestructEntity(DestructEntityEvent.Death event){
        event.setMessageCancelled(true);
    }
}
