package net.modrealms.restrictions.sponge.menu;

import com.codehusky.huskyui.states.Page;
import com.codehusky.huskyui.states.State;
import com.codehusky.huskyui.states.action.ActionType;
import com.codehusky.huskyui.states.action.ClickType;
import com.codehusky.huskyui.states.action.runnable.RunnableAction;
import com.codehusky.huskyui.states.element.ActionableElement;
import net.modrealms.core.object.Menu;
import net.modrealms.core.util.JSONUtil;
import net.modrealms.restrictions.sponge.RestrictionManager;
import net.modrealms.restrictions.sponge.Restrictions;
import net.modrealms.restrictions.sponge.object.Restriction;
import org.apache.commons.lang3.StringUtils;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.property.InventoryDimension;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class RestrictionsMenu extends Menu {

    private final RestrictionManager restrictionManager = Restrictions.getInstance().getRestrictionManager();

    public RestrictionsMenu(Player player) {
        super(player);
        addState(main());
    }

    public State main() {
        Page.PageBuilder builder = this.setup(
                null,
                new InventoryDimension(9, 6),
                Text.of(TextColors.DARK_GRAY, "Restricted Items"),
                true
        );
        
        for (Restriction restriction : restrictionManager.getRestrictionsList().stream().filter(r -> r.isNote() && !r.getWhitelist().contains(Restrictions.getInstance().getCore().getDaoManager().getModpackDAO().getModpack().getTag())).collect(Collectors.toList())) {
            // noted restrictions first
            builder.addElement(new ActionableElement(getRestrictionItemStack(restriction), new RunnableAction(this.container, ClickType.PRIMARY, ActionType.NONE, "", (ele, slot) -> new ClickTask(restriction))));
        }
        for (Restriction restriction : restrictionManager.getRestrictionsList().stream().filter(r -> !r.isNote() && !r.getWhitelist().contains(Restrictions.getInstance().getCore().getDaoManager().getModpackDAO().getModpack().getTag())).collect(Collectors.toList())) {
            // normal banned items after
            if(JSONUtil.getItemStackFromJSON(restriction.getItem()).isPresent()){
                builder.addElement(new ActionableElement(getRestrictionItemStack(restriction), new RunnableAction(this.container, ClickType.PRIMARY, ActionType.NONE, "", (ele, slot) -> new ClickTask(restriction))));
            }
        }
        return builder.build("main");
    }

    private ItemStack getRestrictionItemStack(Restriction restricton){
        ItemStack.Builder item = ItemStack.builder();
        if(restricton.isNote()){
            item.itemType(ItemTypes.PAPER);
            item.add(Keys.DISPLAY_NAME, Text.of(TextColors.RED, "Noted Restriction"));
            item.add(Keys.ITEM_LORE, new ArrayList<Text>(){{
                add(Text.of(TextColors.GRAY, restricton.getReason()));
            }});
        } else {
            Optional<ItemStack> placeholder = JSONUtil.getItemStackFromJSON(restricton.getItem());
            item.fromItemStack(placeholder.get());
            item.add(Keys.ITEM_LORE, new ArrayList<Text>(){{
                add(Text.of(TextColors.GOLD, "Reason: ", TextColors.GRAY, restricton.getReason()));
            }});
        }
        return item.build();
    }

    private class ClickTask implements Runnable {

        private Restriction restriction;

        private ClickTask(Restriction restriction) {
            this.restriction = restriction;
        }

        @Override
        public void run() {
            if (player.hasPermission("modrealms.restrictions.edit.base")) {
                //new EditRestrictionMenu(player, this.restriction).open();
            }
        }
    }
}
