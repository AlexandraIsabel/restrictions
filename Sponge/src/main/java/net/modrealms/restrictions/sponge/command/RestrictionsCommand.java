package net.modrealms.restrictions.sponge.command;

import com.sun.org.apache.regexp.internal.RE;
import net.modrealms.core.util.JSONUtil;
import net.modrealms.restrictions.sponge.Restrictions;
import net.modrealms.restrictions.sponge.menu.RestrictionsMenu;
import net.modrealms.restrictions.sponge.object.Restriction;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class RestrictionsCommand implements CommandCallable {

    private final Restrictions restrictions = Restrictions.getInstance();

    @Override
    public CommandResult process(CommandSource source, String arguments) throws CommandException {
        if (source instanceof Player) {
            if (arguments.isEmpty()) {
                Player player = (Player) source;
                new RestrictionsMenu(player).open();
                return CommandResult.success();
            }
        }

        if (!arguments.isEmpty()) {
            String[] args = arguments.split(" ");
            if(args[0].equalsIgnoreCase("reload")){
                if(source.hasPermission("modrealms.restrictions.reload")){
                    restrictions.getRestrictionManager().sync();
                    restrictions.getSpongeMessenger().sendSuccessMessage(source, "Successfully reloaded the restrictions");
                } else {
                    restrictions.getSpongeMessenger().sendErrorMessage(source, "You don't have permission to reload the restrictions!");
                }
            } else if(args[0].equalsIgnoreCase("add")){
                if(source.hasPermission("modrealms.restrictions.add")){
                    if(source instanceof Player){
                        Player player = (Player) source;
                        if(player.getItemInHand(HandTypes.MAIN_HAND).isPresent()){
                            ItemStack handItem = player.getItemInHand(HandTypes.MAIN_HAND).get();
                            if(args.length >= 2){
                                String reason = arguments.replace((args[0] + " "), "");
                                try {
                                    Restriction restriction = new Restriction();
                                    if(reason.contains("--note")){
                                        restriction.setNote(true);
                                    } else {
                                        if(reason.contains("--exact")){
                                            restriction.setExact(true);
                                        } else {
                                            if(handItem.toContainer().get(DataQuery.of("UnsafeData")).isPresent()){
                                                handItem = ItemStack.builder().fromContainer(handItem.toContainer().remove(DataQuery.of("UnsafeData"))).build();
                                            }
                                            handItem.setQuantity(1);
                                            restriction.setExact(false);
                                        }
                                        // remove astral sorcery tag
                                        handItem.toContainer().remove(DataQuery.of('/', "UnsafeData/ForgeCaps/astralsorcery:cap_item_amulet_holder"));
                                        restriction.setItem(JSONUtil.getItemstackJSON(handItem));
                                    }
                                    restriction.setReason(reason.replace("--note ", "").replace("--exact ", ""));
                                    restrictions.getRestrictionManager().updateRestriction(restriction);
                                    restrictions.getSpongeMessenger().sendSuccessMessage(source, "You have successfully restricted this item on your server!");
                                    return CommandResult.success();
                                } catch (IOException e) {
                                    restrictions.getSpongeMessenger().sendErrorMessage(source, "There was an issue when trying to restrict this item!");
                                    return CommandResult.empty();
                                }
                            } else {
                                restrictions.getSpongeMessenger().sendErrorMessage(source, "You specify a reason to restrict this item!");
                                return CommandResult.empty();
                            }
                        } else {
                            restrictions.getSpongeMessenger().sendErrorMessage(source, "You must have an item in your hand!");
                            return CommandResult.empty();
                        }
                    } else {
                        restrictions.getSpongeMessenger().sendErrorMessage(source, "You must be a player to add a restriction!");
                        return CommandResult.empty();
                    }
                } else {
                    restrictions.getSpongeMessenger().sendErrorMessage(source, "You don't have permission to add a restriction!");
                    return CommandResult.empty();
                }
            } else {
                restrictions.getSpongeMessenger().sendErrorMessage(source, "You haven't specified valid arguments!");
                return CommandResult.empty();
            }
        }
        return CommandResult.empty();
    }

    @Override
    public List<String> getSuggestions(CommandSource source, String arguments, @Nullable Location<World> targetPosition) throws CommandException {
        return null;
    }

    @Override
    public boolean testPermission(CommandSource source) {
        return true;
    }

    @Override
    public Optional<Text> getShortDescription(CommandSource source) {
        return Optional.of(Text.of("Shows you the banned items for your modpack."));
    }

    @Override
    public Optional<Text> getHelp(CommandSource source) {
        return Optional.empty();
    }

    @Override
    public Text getUsage(CommandSource source) {
        return null;
    }
}
