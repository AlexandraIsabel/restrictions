package net.modrealms.restrictions.sponge.event;

import net.modrealms.restrictions.sponge.Restrictions;
import net.modrealms.restrictions.sponge.object.Restriction;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.event.item.inventory.ChangeInventoryEvent;
import org.spongepowered.api.event.item.inventory.DropItemEvent;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Optional;
import java.util.stream.Collectors;

public class ItemDropEvent {
    @Listener(order = Order.FIRST, beforeModifications = true)
    public void onItemDrop(DropItemEvent.Pre event, @Root Player player){
        if(player.hasPermission("modrealms.restrictions.bypass.drop")) return;
        Optional<Restriction> restrictionOptional = Restrictions.getInstance().getRestrictionManager().getRestrictionByItems(event.getOriginalDroppedItems().stream().map(ItemStackSnapshot::createStack).collect(Collectors.toList()));
        if(restrictionOptional.isPresent() && !restrictionOptional.get().getWhitelist().contains(Restrictions.getInstance().getCore().getDaoManager().getModpackDAO().getModpack().getTag()) && restrictionOptional.get().getActions().contains("DROP")){
            event.setCancelled(true);
            player.sendMessage(Text.builder().append(Text.of(TextColors.DARK_RED, "Sorry! You cannot drop this item because it is restricted!"))
                    .append(Text.NEW_LINE)
                    .append(Text.of(TextColors.GOLD, "Reason: ", TextColors.GRAY, restrictionOptional.get().getReason()))
                    .build());
        }
    }
}
